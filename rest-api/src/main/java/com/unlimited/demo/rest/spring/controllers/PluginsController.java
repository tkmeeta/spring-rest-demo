package com.unlimited.demo.rest.spring.controllers;

import com.google.common.io.ByteStreams;
import com.google.common.io.Closeables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.*;

/**
 * Plugins controller.
 * Handle plugins upload.
 *
 * @author Iulian Dumitru
 */
@Controller
public class PluginsController extends RESTController {

	private static final Logger LOG = LoggerFactory.getLogger(PluginsController.class);
	public static final int SIZE_IN_KB = 1024;

	@Value("${plugins.path}")
	private String pluginsDirPath;

	/**
	 * Upload plugin jars.
	 *
	 * @param multipartFile
	 * @param request
	 */
	@RequestMapping(value = "/plugins", method = RequestMethod.POST)
	@ResponseBody
	public void uploadPlugin(@RequestParam("file") MultipartFile multipartFile, HttpServletRequest request) {

		//TODO validate uploaded plugin against existing providers

		try {

			long sizeInKB = multipartFile.getSize() / SIZE_IN_KB;
			String filename = multipartFile.getOriginalFilename();

			LOG.debug("Uploading plugin {} (size {} KB) ...", filename, sizeInKB);

			InputStream inputStream = multipartFile.getInputStream();
			String path = pluginsDirPath + File.separator + filename;
			OutputStream outputStream = new FileOutputStream(path);

			ByteStreams.copy(inputStream, outputStream);

			Closeables.closeQuietly(inputStream);
			Closeables.closeQuietly(outputStream);

			LOG.debug("DONE Uploading plugin jar.", multipartFile);

		} catch (IOException e) {
			LOG.error("Error uploading plugin!", e);
		}

	}

}
