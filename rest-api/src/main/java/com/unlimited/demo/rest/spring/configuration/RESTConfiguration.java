package com.unlimited.demo.rest.spring.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * REST Spring configuration.
 *
 * @author Iulian Dumitru
 */
@EnableWebMvc
@Configuration
@ImportResource("classpath:/security-config.xml")
public class RESTConfiguration {

	public static final int MAX_UPLOAD_SIZE = 10 * 1024 * 1024; //10 MB

	/**
	 * File upload support.
	 *
	 * @return
	 */
	@Bean
	public CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver resolver = new CommonsMultipartResolver();
		resolver.setMaxUploadSize(MAX_UPLOAD_SIZE);
		return resolver;
	}

}
