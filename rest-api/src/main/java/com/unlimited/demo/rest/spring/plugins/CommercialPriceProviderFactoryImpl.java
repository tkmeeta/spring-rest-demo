package com.unlimited.demo.rest.spring.plugins;

import com.unlimited.demo.rest.spring.spi.CommercialPriceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileFilter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Default implementation for {@link CommercialPriceProviderFactory}.
 * Uses a {@link ServiceLoader} to load plugins (jar files added to a directory)
 *
 * @author Iulian Dumitru
 */
@Component
public class CommercialPriceProviderFactoryImpl implements CommercialPriceProviderFactory {

    private static final Logger LOG = LoggerFactory.getLogger(CommercialPriceProviderFactoryImpl.class);

    private static final Map<String, CommercialPriceProvider> providers = new ConcurrentHashMap<String, CommercialPriceProvider>();

    private final String pluginsDirPath;

    @Autowired
    public CommercialPriceProviderFactoryImpl(@Value("${plugins.path}") String pluginsDirPath) {

        this.pluginsDirPath = pluginsDirPath;

        LOG.debug("Creating CommercialPriceProviderFactory using path: {}", pluginsDirPath);

        //verify that the path is a dir
        File dir = new File(pluginsDirPath);

        checkArgument(dir.exists(), "Path " + pluginsDirPath + " does not exist!");
        checkArgument(dir.isDirectory(), "Path " + pluginsDirPath + " does not represent a directory!");

        scanForPlugins();

    }

    /**
     * Scan for new plugins in the plugin directory and add them to providers cache.
     */
    private void scanForPlugins() {

        LOG.info("Scanning for plugins ...");

        ClassLoader parentClassLoader = Thread.currentThread().getContextClassLoader();

        File dir = new File(pluginsDirPath);
        PluginsClassLoader pluginsClassLoader = new PluginsClassLoader(dir, parentClassLoader);

        ServiceLoader<CommercialPriceProvider> serviceLoader = ServiceLoader.load(CommercialPriceProvider.class, pluginsClassLoader);

        for (CommercialPriceProvider commercialPriceProvider : serviceLoader) {
            //ignore channel name's case
            String channelName = commercialPriceProvider.getChannelName().toLowerCase();
            LOG.debug(">>Found provider: {} >> {}", channelName, commercialPriceProvider.getClass().getName());
            providers.put(channelName, commercialPriceProvider);
        }

    }

    public CommercialPriceProvider getCommercialPriceProvider(String channel) {

        LOG.debug("Trying to find a price provider for channel [{}] ...", channel);

        CommercialPriceProvider provider = providers.get(channel.toLowerCase());

        //if there's no provider re-try scanning
        if (provider == null) {
            LOG.debug("Cannot find a price provider for channel [{}]! Will scan for plugins...", channel);
            scanForPlugins();
            provider = providers.get(channel.toLowerCase());
        }

        if (provider == null) {
            LOG.debug("Cannot find a price provider for channel [{}]!", channel);
        } else {
            LOG.debug("Found a price provider for channel [{}].", channel);
        }


        return provider;
    }

    @Override
    public List<CommercialPriceProvider> getProviders() {

        LOG.debug("CommercialPriceProviderFactoryImpl.getProviders() ...");

        Collection<CommercialPriceProvider> collection = providers.values();
        List<CommercialPriceProvider> providers = new ArrayList<CommercialPriceProvider>(collection);

        LOG.debug("DONE CommercialPriceProviderFactoryImpl.getProviders().");

        return providers;

    }

    /**
     * Custom class loader used to load plugin jars from a directory.
     */
    private static final class PluginsClassLoader extends URLClassLoader {

        public static final String JAR_SUFFIX = ".jar";
        public static final URL[] EMPTY_URLS = new URL[]{};

        /**
         * Create a custom classloader that will add to his path every jar in the directory represented by'dirPath'
         *
         * @param dir directory
         */
        public PluginsClassLoader(File dir, ClassLoader parentClassLoader) {
            super(EMPTY_URLS, parentClassLoader);
            init(dir);
        }

        private void init(File dir) {

            try {
                //retrieve all jar files in the dir
                File[] jars = dir.listFiles(new FileFilter() {
                    @Override
                    public boolean accept(File file) {
                        return file.isFile() && file.getName().endsWith(JAR_SUFFIX);
                    }
                });

                //add the jars to classpath
                for (File jar : jars) {
                    URL url = jar.toURI().toURL();
                    LOG.debug("Adding to classpath plugin: {}", url.getFile());
                    addURL(url);
                }

            } catch (MalformedURLException e) {
                LOG.error("Error adding plugin jars from dir: " + dir, e);
            }

        }

    }


}
