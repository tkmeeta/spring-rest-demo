package com.unlimited.demo.rest.spring.controllers;

import com.google.common.base.Strings;
import com.unlimited.demo.rest.domain.Commercial;
import com.unlimited.demo.rest.spring.commons.date.DateUtils;
import com.unlimited.demo.rest.spring.service.CommercialService;
import com.unlimited.demo.rest.spring.service.PageRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Commercials REST controller.
 * Expose channel/commercial resources
 *
 * @author Iulian Dumitru
 */
@Controller
public class CommercialsController extends RESTController {

    private Logger LOG = LoggerFactory.getLogger(CommercialsController.class);

    public static final String DATE_FORMAT = "yyyy.MM.dd.HH.mm.ss";

    private CommercialService service;

    @Autowired
    public CommercialsController(CommercialService service) {
        this.service = service;
    }

    /**
     * Retrieve all commercials on a registred channel channel by criteria.
     * Please note that pagination is not implemented.
     * URL ex:
     * <pre>
     * /api/channels/HBO/commercials?from=2012.09.02.11.08.00&to=2012.09.02.15.00.00&duration=10&occurrencesNr=2&pageNr=1&pageSize=10
     * </pre>
     *
     * @param fromParam          begin date of the commercials (yyyy.MM.dd.HH.mm.ss)
     * @param toParam            end date of the commercials (yyyy.MM.dd.HH.mm.ss)
     * @param durationParam      commercials durations (in seconds)
     * @param occurrencesNrParam number of occurrences
     * @return
     */
    @RequestMapping(value = "/channels/{channel}/commercials", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Commercial> getCommercialsByCriteria(@RequestParam("from") String fromParam,
                                                     @RequestParam("to") String toParam,
                                                     @RequestParam("duration") String durationParam,
                                                     @RequestParam("occurrencesNr") String occurrencesNrParam,
                                                     @RequestParam("pageNr") String pageNrParam,
                                                     @RequestParam("pageSize") String pageSizeParam,
                                                     @PathVariable("channel") String channel) {

        Object[] params = {channel, fromParam, toParam, durationParam, occurrencesNrParam, pageNrParam, pageSizeParam};
        LOG.info("GET /channels/{}/commercials?from={}&to={}&duration={}&occurrencesNr={}&pageNr={}&pageSize={}", params);

        LOG.debug("CommercialsController.getCommercialsByCriteria(fromParam={}, toParam={}, " +
                "durationParam={}, occurrencesNrParam={},pageNrParam={}, pageSizeParam={}) ....", params);

        Date from = getFromParamAsDate(fromParam);
        Date to = getToParamAsDate(toParam);

        checkArgument(to.after(from), "Date " + from + " is after " + to + " !");

        long duration = Long.valueOf(durationParam);
        checkArgument(duration >= 0, "Duration must be at least 1!");

        long occurrencesNr = Long.valueOf(occurrencesNrParam);
        checkArgument(occurrencesNr >= 1, "Occurrences number must at least 1!");

        int pageNr = Integer.valueOf(pageNrParam);
        checkArgument(pageNr >= 1, "Page number must be at least 1!");

        int pageSize = Integer.valueOf(pageSizeParam);
        checkArgument(pageNr >= 1, "Page size number must be at least 1!");

        //pagination is not implemented now for simplicity
        PageRequest pageRequest = new PageRequest(pageNr, pageSize);
        List<Commercial> commercials = service.getCommercials(channel, from, to, duration, occurrencesNr, pageRequest);

        LOG.debug("DONE CommercialsController.getCommercialsByCriteria(fromParam={}, toParam={}, " +
                "durationParam={}, occurrencesNrParam={},pageNrParam={}, pageSizeParam={}).", params);

        return commercials;

    }


    /**
     * Return available channels (/api/channels)
     *
     * @return
     */
    //TODO this could use pagination also
    @RequestMapping(value = "/channels", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<String> getChannels() {

        LOG.info("GET /channels");

        return service.getChannels();

    }

    private Date getToParamAsDate(String toParam) {
        Date to;
        try {
            to = DateUtils.getDateWithFormat(toParam, DATE_FORMAT);
        } catch (Exception e) {
            throw new InputValidationException("Query param 'to=" + toParam + "' is invalid! Please use date format: " + DATE_FORMAT);
        }
        return to;
    }

    private Date getFromParamAsDate(String fromParam) {
        Date from;
        try {
            from = DateUtils.getDateWithFormat(fromParam, DATE_FORMAT);
        } catch (Exception e) {
            throw new InputValidationException("Query param 'from=" + fromParam + "' is invalid! Please use date format: " + DATE_FORMAT);
        }
        return from;
    }


}
