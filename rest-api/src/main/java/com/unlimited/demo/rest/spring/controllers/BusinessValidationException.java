package com.unlimited.demo.rest.spring.controllers;

import com.unlimited.demo.rest.spring.commons.exception.BusinessException;

/**
 * Business validation exception.
 *
 * @author Iulian Dumitru
 */
public class BusinessValidationException extends BusinessException {

    public BusinessValidationException(String message) {
        super(message);
    }

    public BusinessValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}
