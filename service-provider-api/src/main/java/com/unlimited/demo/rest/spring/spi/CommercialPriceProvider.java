package com.unlimited.demo.rest.spring.spi;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Price provider.
 * Defines a computational model for the commercial price on a specific channel.
 *
 * @author Iulian Dumitru
 */
public interface CommercialPriceProvider {

    /**
     * Channel name
     *
     * @return channel name
     */
    String getChannelName();

    /**
     * Computes the commercial price for a specific channel.
     *
     * @param from          start date
     * @param to            end date
     * @param duration      commercial's duration (in seconds)
     * @param occurrencesNr nr of occurrences
     * @return the price for a commercial on a specific channel
     */
    BigDecimal computeCommercialPrice(Date from, Date to, long duration, long occurrencesNr);

}
