package com.unlimited.demo.rest.spring.service;

import com.unlimited.demo.rest.domain.Commercial;

import java.util.Date;
import java.util.List;

/**
 * The service that exposes commercial related operations.
 *
 * @author Iulian Dumitru
 */
public interface CommercialService {

    /**
     * Get a list of commercials price based on specific parameters.
     *
     * @param channel       channel
     * @param from          beginning of the time interval
     * @param to            end of the time interval
     * @param duration      duration for the commercial (in seconds)
     * @param occurrencesNr number of occurrences of the commercial
     */
    List<Commercial> getCommercials(String channel, Date from, Date to, long duration, long occurrencesNr, PageRequest pageRequest)
            throws CommercialNotFoundException;


    /**
     * Return existing channels
     *
     * @return existing channels
     */
    List<String> getChannels();

}
