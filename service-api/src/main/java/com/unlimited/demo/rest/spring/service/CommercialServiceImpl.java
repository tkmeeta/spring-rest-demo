package com.unlimited.demo.rest.spring.service;

import com.unlimited.demo.rest.domain.Commercial;
import com.unlimited.demo.rest.spring.repository.CommercialRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Default implementation for {@link CommercialService}.
 * <p/>
 * This should probably marked as @Transactional
 *
 * @author Iulian Dumitru
 */
@Service
public class CommercialServiceImpl implements CommercialService {

    private Logger LOG = LoggerFactory.getLogger(CommercialServiceImpl.class);

    private CommercialRepository repository;

    @Autowired
    public CommercialServiceImpl(CommercialRepository repository) {
        this.repository = repository;
    }

    /**
     * Retrieve commercials by parameters.
     *
     * @param channel       TV
     * @param from          beginning of the time interval
     * @param to            end of the time interval
     * @param duration      duration for the commercial (in seconds)
     * @param occurrencesNr number of occurrences of the commercial
     * @param pageRequest   pagination support (unused for now)
     * @return a list of commercials
     */
    @Override
    public List<Commercial> getCommercials(String channel, Date from, Date to, long duration, long occurrencesNr, PageRequest pageRequest) {

        Object[] params = {channel, from, to, duration};
        LOG.debug("CommercialServiceImpl.getCommercials(channel={},from={}, to={}, duration={}) ...", params);

        List<Commercial> commercials = repository.getCommercials(channel, from, to, duration, occurrencesNr, pageRequest);
        LOG.debug("DONE CommercialServiceImpl.getCommercials(channel={}, from={}, to={}, duration={}).", params);

        if (commercials == null || commercials.isEmpty()) {
            throw new CommercialNotFoundException("Cannot find commercials for params: channel=" + channel +
                    " from=" + from + " to=" + to + " duration=" + duration);
        }

        return commercials;

    }

    @Override
    public List<String> getChannels() {

        LOG.debug("CommercialServiceImpl.getChannels() ...");

        List<String> channels = repository.getChannels();

        LOG.debug("DONE CommercialServiceImpl.getChannels().");

        return channels;
    }

}
