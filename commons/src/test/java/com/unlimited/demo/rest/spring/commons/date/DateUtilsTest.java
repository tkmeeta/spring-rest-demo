package com.unlimited.demo.rest.spring.commons.date;

import com.unlimited.demo.rest.spring.commons.exception.ParsingException;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

import static org.junit.Assert.assertNotNull;

/**
 * Unit tests for {@link DateUtils}
 *
 * @author Iulian Dumitru
 */
public class DateUtilsTest {

    private static final Logger LOG = LoggerFactory.getLogger(DateUtilsTest.class);

    @Test
    public void testGetDateWithFormat() {

        Date date = DateUtils.getDateWithFormat("2002.01.29.08.36.33", "yyyy.MM.dd.HH.mm.ss");
        assertNotNull(date);

        LOG.debug("date={}", date);


    }

    @Test(expected = ParsingException.class)
    public void testGetDateWithFormatFail() throws Exception {
        DateUtils.getDateWithFormat("q2002.01.29.08.36.33", "yyyy.MM.dd.HH.mm.ss");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetDateWithFormatBadInput() throws Exception {
        DateUtils.getDateWithFormat(null, "yyyy.MM.dd.HH.mm.ss");
    }

}
